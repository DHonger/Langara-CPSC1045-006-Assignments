//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
function drawBoxes(){
    let svgString = '<svg width="2000" height="2000">';
    let boxes = Number(document.getElementById("boxNumber").value;
    let size =Number(document.getElementById("initialSize").value);
    let growth = Number(document.getElementById("Growth").value);
    let x = 50
  
    for (  ; boxes>0; boxes-- ){
          let squareString = '<rect x="'+x+'"'+' y="'+300+'" width="'+size+'"'+'height="'+size+'" fill="black" />';
        let drawString = squareString;
        svgString += drawString;
        size = size+growth
        x = x+100
   
    }
svgString += "</svg>";
console.log(svgString)
console.log(boxes)
    let box = document.querySelector("#box");
    box.innerHTML = svgString;

window.drawBoxes = drawBoxes