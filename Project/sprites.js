console.log("Sprite file is connected")


function drawAnimal(x,y){
    let animal = '<g id=animal transform="translate('+x+' '+y+')"><ellipse cx="100" cy="80" rx="100" ry="50" style="fill:orange;stroke:"orange"  /> <polygon points="150,80 250,150 250,10" style="fill:orange;stroke:"orange" /> <circle cx= "50" cy="60" r="10" /></g>'
    return animal;
}

function drawFruit(x,y){
    let fruit = '<g id=fruit transform="translate('+x+' '+y+')"><circle cx="50" cy="50" r="40" stroke="black" stroke-width="2" fill="orange" /></g>'
    return fruit;
}
// its an orange

function drawVehicle(x,y){
    let vehicle ='<g id=vehicle transform="translate('+x+' '+y+')"> <rect width="300" height="150" x="40" y="10" style="fill:blue;stroke-width:3;stroke:black" /><circle cx="105" cy="155" r="30" /><circle cx="275" cy="155" r="30" /></g>'
    return vehicle;
}
function drawPlant(x,y){
    let plant = '<g id=plant transform="translate('+x+' '+y+')"><rect x="60" y="75" width="75" height="300" style="fill:brown;stroke-width:3;stroke:rgb(0,0,0)" /><circle cx="100" cy="100" r="80" style="fill:green;stroke-width:3;stroke:black;" /></g>'
    return plant
}

window.onload = function() {
    let animalString = drawAnimal(400,50);
    let fruitString = drawFruit(200,200);
    let vehicleString = drawVehicle(400,500);
    let plantString = drawPlant(0,0);
    document.querySelector("#svgCanvas").innerHTML += animalString;
    document.querySelector("#svgCanvas").innerHTML += fruitString;
    document.querySelector("#svgCanvas").innerHTML += vehicleString;
    document.querySelector("#svgCanvas").innerHTML += plantString;
}