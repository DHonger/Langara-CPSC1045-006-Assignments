//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
function update(){
    let MyX = document.querySelector("#x-axis");
    let MyY = document.querySelector("#y-axis");
    let Rotate = document.querySelector("#rotation");
    let a = Number(MyX.value);
    let b = Number(MyY.value);
    let c = Number(Rotate.value);
    let d = 200+a
    let e = 200+b
    let translateString = "translate("+a +" "+ b+")";
    console.log(translateString);
    let rotateString = "rotate("+c +" "+ d +" "+ e+")";
    let transformString = translateString + rotateString
    collection.setAttribute("transform",transformString);
}

function change(){
    console.log("Running!");
    let inR = document.querySelector("#innerRadius");
    let outR = document.querySelector("#outerRadius");
    let starx = document.querySelector("#starx");
    let stary = document.querySelector("#stary");
    let Rin = Number(inR.value)
    let Rout = Number(outR.value)

    let x1 = Rout * Math.cos(0)
    let x2 = Rin * Math.cos(0.76)
    let x3 = Rout * Math.cos(1.57)
    let x4 = Rin * Math.cos(2.36)
    let x5 = Rout * Math.cos(3.14)
    let x6 = Rin * Math.cos(3.92)
    let x7 = Rout * Math.cos(4.71)
    let x8 = Rin * Math.cos(5.5)

    let y1 = Rout * Math.sin(0)
    let y2 = Rin * Math.sin(0.76)
    let y3 = Rout * Math.sin(1.57)
    let y4 = Rin * Math.sin(2.36)
    let y5 = Rout * Math.sin(3.14)
    let y6 = Rin * Math.sin(3.92)
    let y7 = Rout * Math.sin(4.71)
    let y8 = Rin * Math.sin(5.5)

    let xVal = Number(starx.value)
    let yVal = Number(stary.value)
    let translateStar = "translate("+xVal +" "+ yVal+")";

    let pointsign = (x1 + " " + y1 + " " + x2 + " " + y2 + " " + x3 + " " + y3 + " " + x4 + " " + y4 + " " + x5 + " " + y5 + " " + x6 + " " + y6 + " " + x7 + " " + y7 + " " + x8 + " " + y8)
   
   console.log(pointsign);
   console.log("hello");
    star.setAttribute("points",pointsign);
    star.setAttribute("transform",translateStar);
    console.log("end");
}
window.update = update
window.change = change