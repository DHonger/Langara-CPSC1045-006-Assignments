/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
function update(){
    let MyX = document.querySelector("#x-axis");
    let MyY = document.querySelector("#y-axis");
    let Rotate = document.querySelector("#rotation");
    let a = Number(MyX.value);
    let b = Number(MyY.value);
    let c = Number(Rotate.value);
    let d = 200+a
    let e = 200+b
    let translateString = "translate("+a +" "+ b+")";
    console.log(translateString);
    let rotateString = "rotate("+c +" "+ d +" "+ e+")";
    let transformString = translateString + rotateString
    collection.setAttribute("transform",transformString);
}

function change(){
    console.log("Running!");
    let inR = document.querySelector("#innerRadius");
    let outR = document.querySelector("#outerRadius");
    let starx = document.querySelector("#starx");
    let stary = document.querySelector("#stary");
    let Rin = Number(inR.value)
    let Rout = Number(outR.value)

    let x1 = Rout * Math.cos(0)
    let x2 = Rin * Math.cos(0.76)
    let x3 = Rout * Math.cos(1.57)
    let x4 = Rin * Math.cos(2.36)
    let x5 = Rout * Math.cos(3.14)
    let x6 = Rin * Math.cos(3.92)
    let x7 = Rout * Math.cos(4.71)
    let x8 = Rin * Math.cos(5.5)

    let y1 = Rout * Math.sin(0)
    let y2 = Rin * Math.sin(0.76)
    let y3 = Rout * Math.sin(1.57)
    let y4 = Rin * Math.sin(2.36)
    let y5 = Rout * Math.sin(3.14)
    let y6 = Rin * Math.sin(3.92)
    let y7 = Rout * Math.sin(4.71)
    let y8 = Rin * Math.sin(5.5)

    let xVal = Number(starx.value)
    let yVal = Number(stary.value)
    let translateStar = "translate("+xVal +" "+ yVal+")";

    let pointsign = (x1 + " " + y1 + " " + x2 + " " + y2 + " " + x3 + " " + y3 + " " + x4 + " " + y4 + " " + x5 + " " + y5 + " " + x6 + " " + y6 + " " + x7 + " " + y7 + " " + x8 + " " + y8)
   
    star.setAttribute("points",pointsign);
    star.setAttribute("transform",translateStar);
    console.log("end");
}
window.update = update
window.change = change

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQiIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXguanNcIik7XG4iLCIvL1RoaXMgaXMgdGhlIGVudHJ5IHBvaW50IEphdmFTY3JpcHQgZmlsZS5cclxuLy9XZWJwYWNrIHdpbGwgbG9vayBhdCB0aGlzIGZpbGUgZmlyc3QsIGFuZCB0aGVuIGNoZWNrXHJcbi8vd2hhdCBmaWxlcyBhcmUgbGlua2VkIHRvIGl0LlxyXG5mdW5jdGlvbiB1cGRhdGUoKXtcclxuICAgIGxldCBNeVggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3gtYXhpc1wiKTtcclxuICAgIGxldCBNeVkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3ktYXhpc1wiKTtcclxuICAgIGxldCBSb3RhdGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3JvdGF0aW9uXCIpO1xyXG4gICAgbGV0IGEgPSBOdW1iZXIoTXlYLnZhbHVlKTtcclxuICAgIGxldCBiID0gTnVtYmVyKE15WS52YWx1ZSk7XHJcbiAgICBsZXQgYyA9IE51bWJlcihSb3RhdGUudmFsdWUpO1xyXG4gICAgbGV0IGQgPSAyMDArYVxyXG4gICAgbGV0IGUgPSAyMDArYlxyXG4gICAgbGV0IHRyYW5zbGF0ZVN0cmluZyA9IFwidHJhbnNsYXRlKFwiK2EgK1wiIFwiKyBiK1wiKVwiO1xyXG4gICAgY29uc29sZS5sb2codHJhbnNsYXRlU3RyaW5nKTtcclxuICAgIGxldCByb3RhdGVTdHJpbmcgPSBcInJvdGF0ZShcIitjICtcIiBcIisgZCArXCIgXCIrIGUrXCIpXCI7XHJcbiAgICBsZXQgdHJhbnNmb3JtU3RyaW5nID0gdHJhbnNsYXRlU3RyaW5nICsgcm90YXRlU3RyaW5nXHJcbiAgICBjb2xsZWN0aW9uLnNldEF0dHJpYnV0ZShcInRyYW5zZm9ybVwiLHRyYW5zZm9ybVN0cmluZyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNoYW5nZSgpe1xyXG4gICAgY29uc29sZS5sb2coXCJSdW5uaW5nIVwiKTtcclxuICAgIGxldCBpblIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2lubmVyUmFkaXVzXCIpO1xyXG4gICAgbGV0IG91dFIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI291dGVyUmFkaXVzXCIpO1xyXG4gICAgbGV0IHN0YXJ4ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzdGFyeFwiKTtcclxuICAgIGxldCBzdGFyeSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc3RhcnlcIik7XHJcbiAgICBsZXQgUmluID0gTnVtYmVyKGluUi52YWx1ZSlcclxuICAgIGxldCBSb3V0ID0gTnVtYmVyKG91dFIudmFsdWUpXHJcblxyXG4gICAgbGV0IHgxID0gUm91dCAqIE1hdGguY29zKDApXHJcbiAgICBsZXQgeDIgPSBSaW4gKiBNYXRoLmNvcygwLjc2KVxyXG4gICAgbGV0IHgzID0gUm91dCAqIE1hdGguY29zKDEuNTcpXHJcbiAgICBsZXQgeDQgPSBSaW4gKiBNYXRoLmNvcygyLjM2KVxyXG4gICAgbGV0IHg1ID0gUm91dCAqIE1hdGguY29zKDMuMTQpXHJcbiAgICBsZXQgeDYgPSBSaW4gKiBNYXRoLmNvcygzLjkyKVxyXG4gICAgbGV0IHg3ID0gUm91dCAqIE1hdGguY29zKDQuNzEpXHJcbiAgICBsZXQgeDggPSBSaW4gKiBNYXRoLmNvcyg1LjUpXHJcblxyXG4gICAgbGV0IHkxID0gUm91dCAqIE1hdGguc2luKDApXHJcbiAgICBsZXQgeTIgPSBSaW4gKiBNYXRoLnNpbigwLjc2KVxyXG4gICAgbGV0IHkzID0gUm91dCAqIE1hdGguc2luKDEuNTcpXHJcbiAgICBsZXQgeTQgPSBSaW4gKiBNYXRoLnNpbigyLjM2KVxyXG4gICAgbGV0IHk1ID0gUm91dCAqIE1hdGguc2luKDMuMTQpXHJcbiAgICBsZXQgeTYgPSBSaW4gKiBNYXRoLnNpbigzLjkyKVxyXG4gICAgbGV0IHk3ID0gUm91dCAqIE1hdGguc2luKDQuNzEpXHJcbiAgICBsZXQgeTggPSBSaW4gKiBNYXRoLnNpbig1LjUpXHJcblxyXG4gICAgbGV0IHhWYWwgPSBOdW1iZXIoc3RhcngudmFsdWUpXHJcbiAgICBsZXQgeVZhbCA9IE51bWJlcihzdGFyeS52YWx1ZSlcclxuICAgIGxldCB0cmFuc2xhdGVTdGFyID0gXCJ0cmFuc2xhdGUoXCIreFZhbCArXCIgXCIrIHlWYWwrXCIpXCI7XHJcblxyXG4gICAgbGV0IHBvaW50c2lnbiA9ICh4MSArIFwiIFwiICsgeTEgKyBcIiBcIiArIHgyICsgXCIgXCIgKyB5MiArIFwiIFwiICsgeDMgKyBcIiBcIiArIHkzICsgXCIgXCIgKyB4NCArIFwiIFwiICsgeTQgKyBcIiBcIiArIHg1ICsgXCIgXCIgKyB5NSArIFwiIFwiICsgeDYgKyBcIiBcIiArIHk2ICsgXCIgXCIgKyB4NyArIFwiIFwiICsgeTcgKyBcIiBcIiArIHg4ICsgXCIgXCIgKyB5OClcclxuICAgXHJcbiAgICBzdGFyLnNldEF0dHJpYnV0ZShcInBvaW50c1wiLHBvaW50c2lnbik7XHJcbiAgICBzdGFyLnNldEF0dHJpYnV0ZShcInRyYW5zZm9ybVwiLHRyYW5zbGF0ZVN0YXIpO1xyXG4gICAgY29uc29sZS5sb2coXCJlbmRcIik7XHJcbn1cclxud2luZG93LnVwZGF0ZSA9IHVwZGF0ZVxyXG53aW5kb3cuY2hhbmdlID0gY2hhbmdlIl0sInNvdXJjZVJvb3QiOiIifQ==