//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.

function changeRadii(){
    let MyVar = document.querySelector("#inputR")
    let a = Number(MyVar.value)
    let c = (200-2*a);
    let d = 200-c;
    let circle = document.querySelector("#left-circle")
    circle.setAttribute("r", c)
    let circle2 = document.querySelector("#right-circle")
    circle2.setAttribute("r", d)
    window.changeRadii = changeRadii;
}