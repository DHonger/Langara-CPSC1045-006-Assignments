//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.

document.getElementById("svg").addEventListener("click", createCircle);
document.getElementById("exerciseSVG").addEventListener("click", addPolyline);

function createCircle() {
    let c = document.querySelector("#color").value;
    let x = event.offsetX;
    let y = event.offsetY;
    console.log(x);
    console.log(y);
    console.log(c);
    let circleString = "<circle cx='" + x + "' cy='" + y + "' r=20 fill='" + c + "' />";
    console.log(circleString);
    svg.innerHTML += circleString;

}

function addPolyline() {
    let polyPoint = document.querySelector("#exerciseSVG>polyline:last-of-type");
    let points = polyPoint.getAttribute("points");
    let pointX = event.offsetX;
    let pointY = event.offsetY;
    points = points + " " + pointX + "," + pointY;
    polyPoint.setAttribute("points", points);
}

function newPoly() {
    let polyString = '<polyline points="" stroke="black" fill="none" stroke-width="5" />';
    exerciseSVG.innerHTML += polyString;
}

let color = ["red", "blue", "pink", "green"];
let j = 0;

function changeColor() {
    let polyline = document.querySelectorAll("#exerciseSVG>polyline");
    // console.log(polyline);
    // console.log("this is changeColor");
    for (i = 0; i < polyline.length; i++) {
        // console.log(i);
        // console.log(polyline[i]);
        polyline[i].setAttribute("stroke", color[j]);
    }
}
// setInterval(function () {
//     changeColor();
//     j++
//     if (j > 3) {
//         j = 0
//     }
// }, 100)
setInterval(function () {
    changeColor();
    j++;
    if (j > 3)
        j = 0;

}, 1000);
// setInterval(changeColor, 100);
window.createCircle = createCircle;
window.addPolyline = addPolyline;
window.newPoly = newPoly;
window.changeColor = changeColor;

